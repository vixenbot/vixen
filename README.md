# Vixen Discord Bot
This main repo holds all of the submodules for Vixen. The way it is intended to be used is by cloning this repo and not the submodules directly. Instructions are below.
<br/><br/>

# Table of Contents
- [Prerequisites](#prerequisites)
- [Configuration](#configuration)
- [Usage](#usage)
  - [Production](#production-usage)
  - [Development](#development-usage)
- [License](#license)
<br/><br/>

# Prerequisites
Vixen uses Docker to host all of its components, therefore you need to ensure you have Docker and docker-compose installed on the system which you intend to run Vixen.

- Docker 19.03.12 or later (recommended version)
- docker-compose 1.26.2 or later (recommended version)
<br/><br/>

# Configuration
Before starting the stack, a configuration file needs to be made for the bot module. In the `bot` subdirectory, simply put a file named `config.json` with the following format:
```json
// bot/config.json
{
  "token": "",        // Discord token
  "owner": "",        // Discord ID of the bot owner, if applicable
  "clientId": "",     // Discord Client ID
  "clientSecret": "", // Discord Client Secret
}
```
At this time, this is the only extra configuration required for Vixen. This could change in the future.
<br/><br/>

# Usage
Vixen has two different docker-compose files for different use cases. If you're only interested in running Vixen, then you need only worry about one of them. If you're wanting to further develop Vixen, allow me to explain what both compose files are for.
<br/><br/>

## Production Usage
For production usage, the `production-compose.yml` file is used. The main difference between this compose file and the other is the absence of config settings that are helpful during development, but unneeded or unwanted in production.
```bash
docker-compose -f production-compose.yml up -d
```
As said before, the only file you need to worry about if you're only going to be using Vixen and not developing it.
<br/><br/>

## Development Usage
For development, the `development-compose.yml` file is used. This version of the compose file contains extra configuration settings that are intended to assist development.
```bash
docker-compose -f development-compose.yml up -d
```
Keep in mind that this configuration exposes ports on the host machine that you probably won't want exposed in a production environment, such as ports to access the backend database. In a production environment, these ports don't need to be exposed since the only communication that needs to happen is done over the defined Docker networks.
<br/><br/>

# License
The Vixen Discord bot is licensed under the GNU GPLv3 license. See [LICENSE](/LICENSE) for more info.